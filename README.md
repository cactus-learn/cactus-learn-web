# Comme un cactus (Web VueJS)
Dans le cadre de notre projet de fin d'études au sein de la filière  Architecture des logiciels au sein de l'ESGI, nous avons décidé de faire un projet qui a pour but :  
La mise en place d'une plateforme de préparation en ligne aux tests, aux examens et d'apprentissage. La plateforme permettra de mettre en lien des intervenant/bénévole et des étudiants, lycéens, collégiens ou toute personne qui voudrait solliciter une aide particulière.   
Cette solution permettra de mêler un enseignement traditionnel avec de l'E-learning. Permettant ainsi le transfert de cours, la mise en place d'exercice, et une plateforme d'échange entre les différentes personnes d'une même organisation.

 Projet réalisé par : 
 - Ourdia Oualiken
 - Joseph Chatelin
 - Djadji Traore   

Avec comme responsable de filière **Mr. Sananes**

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Testing with jest
```
npm run test
```
