
const authUrl = "auth/"
const exerciseUrl = "exercise/"
const responseUrl = "send-response"
const userUrl = "user/"
const correctionUrl = "correction/"
const createUrl = "create/"
const groupeUrl = "groupe/"
const groupeStatsUrl = "statistics/groupe/"
const forumUrl = "forum/"
const organisationUrl = "organisation/"
const lessonUrl = "lesson/"
const messageUrl = "message/"
const likeUrl = "updateLikeStatut/"
const userResponseUrl = "responses/"
const contactUrl = "myContacts"
const userInfoUrl = "info"
const accountElevationUrl = "sendCreatorRequest/"
const passwordUrl = "updatePassword/"
const addMemberUrl = "addMember/"
const removeMemberUrl = "removeMember/"
const loginUrl = "login/"
const registerUrl = "register/"
const isAdminUrl = "/isAdmin/"
const forgotPassword = "/password/forgot"

const headers = {
    'Content-Type': 'application/json',
}

export default {
    exerciseUrl: exerciseUrl,
    responseUrl: responseUrl,
    userUrl: userUrl,
    correctionUrl: correctionUrl,
    createUrl: createUrl,
    groupeUrl: groupeUrl,
    groupeStatsUrl: groupeStatsUrl,
    forumUrl: forumUrl,
    lessonUrl: lessonUrl,
    organisationUrl: organisationUrl,
    authUrl: authUrl,
    messageUrl: messageUrl,
    likeUrl: likeUrl,
    userResponseUrl: userResponseUrl,
    contactUrl: contactUrl,
    userInfoUrl: userInfoUrl,
    accountElevationUrl: accountElevationUrl,
    passwordUrl: passwordUrl,
    addMemberUrl: addMemberUrl,
    removeMemberUrl : removeMemberUrl,
    loginUrl: loginUrl,
    registerUrl: registerUrl,
    isAdminUrl: isAdminUrl,
    forgotPassword: forgotPassword,
    headers: headers
}