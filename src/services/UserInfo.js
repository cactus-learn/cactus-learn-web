
import Vue from 'vue'
import VueCryptojs from 'vue-cryptojs'

Vue.use(VueCryptojs)

export default {
  getUserToken() {
    let encryptedToken = localStorage.token
    return this.CryptoJS.AES.decrypt(encryptedToken, process.env.VUE_APP_SECRET_PHRASE).toString(this.CryptoJS.enc.Utf8)
  }
}