import Vue from 'vue'
import Vuetify from 'vuetify/lib'

Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#164A41',
        secondary: '#4D774E',
        accent: '#F1B24A', 
      },
    },
  },
})