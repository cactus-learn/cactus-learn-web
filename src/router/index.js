import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Course from '../views/Course.vue'
import ExerciseStudent from '../views/ExerciseStudent.vue'
import ExerciseResponseTeacher from '../views/ExerciseResponseTeacher'
import Exercise from '../views/Exercise.vue'
import LessonDetails from '../views/LessonDetails.vue'
import GroupeList from '../views/GroupeList.vue'
import UserResponse from '../views/UserResponse.vue'
import ForumList from '../views/ForumList.vue'
import ForumDetails from '../views/ForumDetails'
import Message from '../views/Message.vue'
import Profil from '../views/Profil.vue'
import WelcomePage from '../views/WelcomePage.vue'
import PageNotFound from '../views/PageNotFound.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Accueil',
    component: Home
  },
  {
    path: '/welcome/',
    name: 'Bienvenue',
    component: WelcomePage
  },
  {
    path: '/course',
    name: 'Liste de cours',
    component: Course,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/exercise-student/:id',
    name: 'Exercices utilisateur',
    component: ExerciseStudent,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/exercise-teacher/:id',
    name: 'Liste des réponses d\'un exercice, pour un enseignant',
    component: ExerciseResponseTeacher,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/exercise-response/:id',
    name: 'Réponse utilisateur',
    component: UserResponse,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/exercise/:id',
    name: 'Exercice',
    component: Exercise,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/lesson/:id',
    name: 'Détails du cours',
    component: LessonDetails,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/organisation/:id',
    name: 'Liste des groupes',
    component: GroupeList,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/forum/:organisationId/:groupeId',
    name: 'Forum du groupe',
    component: ForumList,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/forum/:forumId',
    name: 'Détail du Forum',
    component: ForumDetails,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/message',
    name: 'Message',
    component: Message,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/profil',
    name: 'Profil Utilisateur',
    component: Profil,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '*',
    name: 'Page inconnu',
    component: PageNotFound,
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
      if (localStorage.token == null || localStorage.token == undefined) {
          next({
              path: '/',
          })
      } else {
        next()
      }
  }else {
      next()
  }
})


export default router
