import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import router from './router'
import VueCryptojs from 'vue-cryptojs'
import VueCoreVideoPlayer from 'vue-core-video-player';
import VueAudio from 'vue-audio-better'
 

Vue.config.productionTip = false
 
Vue.use(VueCryptojs)
Vue.use(VueCoreVideoPlayer)
Vue.use(VueAudio)


new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
